package misc;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Scraping {
	
	public WebDriver driver=null;
	public String chromeDriverpath = System.getProperty("user.dir") + "\\drivers\\chromedriver.exe";
	
	@Test
	public void scraping()
	{
		boolean pagination=true;
		try
		{
			System.setProperty("webdriver.chrome.driver", chromeDriverpath );
			driver = new ChromeDriver();
			
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			driver.get("https://bank-code.net/uk-sort-code/bank/halifax");
			
			
			while (pagination==true)
			{
				
				//System.out.println("Pagination boolean " + pagination);
				//System.out.println();
				List<WebElement> sortCodes =driver.findElements(By.cssSelector("table#uk-sort-codes tbody tr"));
				
				for (WebElement r : sortCodes)
				{
					if (r.findElements(By.cssSelector("td")).size()>0)
					{
						List <WebElement> cols = r.findElements(By.cssSelector("td"));
						for (WebElement c : cols)
							System.out.print ( c.getText() + ", ");
						System.out.print("\n");
					}
				}
				
				List<WebElement> paginations = driver.findElements(By.cssSelector("div#uk-sort-codes_paginate ul li a"));
				
				for (WebElement e: paginations)
				{
					//System.out.println(e.getText());
					if (!e.getText().trim().equalsIgnoreCase("Next"))
					{
						pagination=false;
					}
					else if(e.getText().trim().equalsIgnoreCase("Next"))
					{
						pagination = true;
						e.click();
					}
					
				}
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		//driver.close();
	}

}

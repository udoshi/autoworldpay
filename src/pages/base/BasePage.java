package pages.base;

import java.awt.Menu;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
//import com.sun.security.auth.login.ConfigFile;

import instarem.utilities.Constants;
import instarem.utilities.ExtentManager;
import instarem.utilities.TestUtils;

public class BasePage {

	protected final RemoteWebDriver driver;
	// public Logger log4j =null;
	protected ExtentTest extentTest = null;
	private boolean acceptNextAlert = true;
	private String page = "";
	private String upcomingYearSelect = "//div[@class='react-datepicker__year-dropdown']/div[7]";
	private String upcomingYearsArrow = "//a[@class='react-datepicker__navigation react-datepicker__navigation--years react-datepicker__navigation--years-upcoming']";
	private String yearDropdown = "//span[@class='react-datepicker__year-read-view--down-arrow']";
	private String prevYearSelect = "//div[@class='react-datepicker__year-dropdown']/div[2]";
	private String previousYearsArrow = "//a[@class='react-datepicker__navigation react-datepicker__navigation--years react-datepicker__navigation--years-previous']";
	private String currentYearSelect = "//div[@class='react-datepicker__year-dropdown']/div[1]";
	public Properties properties;

	public BasePage(RemoteWebDriver _driver, ExtentTest _extentTest, String _page) {
		this.driver = _driver;
		this.extentTest = _extentTest;
		this.page = _page;
	}

	protected WebElement getElementWhenVisible(By locator, int timeout) {
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			Reporter.log(page + ": Element is visible : " + locator.toString());
			// extentTest.log(LogStatus.PASS, page + ": Element is visible " +
			// element);
		} catch (Exception e) {
			// System.out.println("Element not found --> "+ e.toString() +
			// e.getMessage());
			Reporter.log(page + ": Exception in getElementWhenVisible() : " + e.toString() + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getElementWhenVisible(): " + locator.toString() + "\n " +
			// e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return element;
	}

	protected boolean getElementInvisibility(By locator, int timeout) {
		boolean invisible = false;

		try {
			WebDriverWait dWait = new WebDriverWait(driver, timeout);
			invisible = dWait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
			// System.out.println("getElementInvisibility(): " + invisible + "
			// FOR: " + locator);
			Reporter.log(page + "Element is invisible: " + locator.toString());
		} catch (Exception e) {
			Reporter.log(page + "Exception in getElementInvisibility()" + locator.toString());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getElementInvisibility(): " + locator.toString() + "\n " +
			// e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return invisible;
	}

	// tablename - CSS selectory till TR
	public int getTotalTableRows(By tableName) {
		int totalRows = 0;
		try {
			List<WebElement> tRows = driver.findElements(tableName);
			totalRows = tRows.size();
			// System.out.println("Total rows : " + totalRows);
		} catch (Exception e) {
			// System.out.println();
			Reporter.log(page + "Exception in getTotalTableRows(): " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getTotalTableRows(): " + tableName.toString() + "\n " +
			// e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return totalRows;
	}

	protected int lookForDataInTable(By tableName, String lookForData) {
		int rowIndex = 1;
		try {
			List<WebElement> tableRows = driver.findElements(tableName);
			// System.out.println("table rows: " + tableRows.size());

			for (WebElement r : tableRows) {
				// List<WebElement> tableCols =
				// r.findElements(By.cssSelector("td"));
				// System.out.println("ROW data --> " + r.getText());
				if (r.getText().trim().contains(lookForData))
					return rowIndex;
				else
					rowIndex++;
			}
		} catch (Exception e) {
			// System.out.println("Exception in lookForDataInTable(): " +
			// e.getMessage());
			Reporter.log(page + "Exception in lookForDataInTable(): " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// lookForDataInTable(): " + tableName.toString() + "\n " +
			// e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return rowIndex;
	}

	public void ConfigFileReader() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(Constants.propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + Constants.propertyFilePath);
		}
	}

	protected void selectDateold(String day, String Month) throws Exception {
		try {
			// click to open the date time picker calendar.
			// Provide the day of the month to select the date.
			// click to open the date time picker calendar.
			// Provide the day of the month to select the date.
			ConfigFileReader();
			Thread.sleep(2000);
			System.out.println(properties.getProperty("MonthDropdown"));
			String element = properties.getProperty("MonthDropdown");
			WebElement MonthDropDown = driver.findElement(By.xpath(element));
			MonthDropDown.click();
			String MonthValue = properties.getProperty("SelectMonth") + Month + "]";
			WebElement SelectMonth = driver.findElement(By.xpath(MonthValue));
			// Thread.sleep(1000);
			SelectMonth.click();
			String DayValue = properties.getProperty("SelectDay") + day + "')]";
			By calendarXpath = By.xpath(DayValue);
			driver.findElement(calendarXpath).click();

			// Intentional pause for 2 seconds.
			Thread.sleep(2000);
		} catch (Exception e) {
			Reporter.log(page + ": Exception in selectListOption() : " + e.getMessage());
		}
	}

	protected void selectDate(String day, String Month, String year) throws Exception {
		try {
			// click to open the date time picker calendar.
			// Provide the day of the month to select the date.
			// click to open the date time picker calendar.
			// Provide the day of the month to select the date.
			ConfigFileReader();
			driver.findElement(By.xpath(yearDropdown)).click();
			// Thread.sleep(3000);
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			int getyear = Integer.parseInt(year);
			if (getyear > currentYear) {
				for (int k = 0; k < 80; k++) {
					WebElement ele = driver.findElement(By.xpath(upcomingYearSelect));
					String po = ele.getText();
					if (po.contains(year)) {
						ele.click();
						break;
					} else {
						driver.findElement(By.xpath(upcomingYearsArrow)).click();
					}
				}
			} else if (getyear < currentYear) {

				for (int k = 0; k < 80; k++) {
					WebElement ele = driver.findElement(By.xpath(prevYearSelect));
					String jk = ele.getText();
					if (jk.contains(year)) {
						ele.click();
						break;
					} else {
						driver.findElement(By.xpath(previousYearsArrow)).click();
					}
				}
			} else {
				driver.findElement(By.xpath(currentYearSelect)).click();
			}

			System.out.println(properties.getProperty("MonthDropdown"));
			String element = properties.getProperty("MonthDropdown");
			WebElement MonthDropDown = driver.findElement(By.xpath(element));
			MonthDropDown.click();
			String MonthValue = properties.getProperty("SelectMonth") + Month + "]";
			WebElement SelectMonth = driver.findElement(By.xpath(MonthValue));
			// Thread.sleep(1000);
			SelectMonth.click();
			String DayValue = properties.getProperty("SelectDay") + day + "')]";
			By calendarXpath = By.xpath(DayValue);
			driver.findElement(calendarXpath).click();

			// Intentional pause for 2 seconds.
			//Thread.sleep(2000);
		} catch (Exception e) {
			Reporter.log(page + ": Exception in selectListOption() : " + e.getMessage());
		}
	}

	protected String getDateTime() {
		Date now = new Date();
		// String datetimeStr = now.toString();
		SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss");
		// System.out.println(format.format(now));
		return (format.format(now));
	}

	protected void selectDateSch(String day, String Month) throws Exception {
		try {
			// click to open the date time picker calendar.
			// Provide the day of the month to select the date.
			ConfigFileReader();
			Thread.sleep(2000);
			System.out.println(properties.getProperty("MonthDropdown"));
			String element = properties.getProperty("MonthDropdown");
			WebElement MonthDropDown = driver.findElement(By.xpath(element));
			MonthDropDown.click();
			String MonthValue = properties.getProperty("SelectMonth") + Month + "]";
			WebElement SelectMonth = driver.findElement(By.xpath(MonthValue));
			SelectMonth.click();
			System.out.println("month cliked");
			String DayValue = properties.getProperty("SelectDay") + day + "')]";
			List<WebElement> list = driver.findElements(By.xpath("//div[@class='react-datepicker__month']/div"));
			int size1 = list.size();
			System.out.println(size1);
			for (int i = 0; i < size1; i++) {
				System.out.println(list.get(i).getText());
				if (list.get(i).getText().contains(day)) {
					list.get(i).click();
				}
			}
			Thread.sleep(2000);

		} catch (Exception e) {
			Reporter.log(page + ": Exception in selectListOption() : " + e.getMessage());
		}
	}

	public String getRowColValue(String tableId, int rowIndex, int colIndex) throws IOException {
		String colValue = "";
		List<WebElement> getTableElements = null;
		WebDriverWait waiting = new WebDriverWait(driver, 3);
		By Element = TestUtils.getLocator(tableId);
		String Value = Element.toString().split(": ")[1];
		try {
			By byRowCol = By.xpath("//table[@id='" + Value + "']//tbody//tr[" + rowIndex + "]//td[" + colIndex + "]");
			colValue = driver.findElement(byRowCol).getText();
		} catch (Exception e) {
			Reporter.log(page + "Exception in getRowColValue(): " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getRowColValue(): " + tableId + "\n " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}

		// System.out.println("Column value: " + colValue);
		return colValue;
	}

	public WebElement getRowColElement(String tableId, int rowIndex, int colIndex) {
		WebElement element = null;
		try {
			By byRowCol = By.xpath("//table[@id='" + tableId + "']//tr[" + rowIndex + "]//td[" + colIndex + "]");
			element = driver.findElement(byRowCol);
		} catch (Exception e) {
			Reporter.log(page + "Exception in getRowColElement(): " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getRowColElement(): " + tableId + "\n " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return element;
	}

	public String getTextFromTable(String element, int SearchColNUm, String TextToSearch, int actionCol)
			throws IOException {
		List<WebElement> getTableElements = null;
		WebDriverWait waiting = new WebDriverWait(driver, 3);
		By Element = TestUtils.getLocator(element);
		String Value = Element.toString().split(": ")[1];
		String Text = null;
		try {
			getTableElements = driver
					.findElements(By.xpath("//table[@id='" + Value + "']//tbody//tr//td[" + SearchColNUm + "]"));
			for (int i = 0; i <= getTableElements.size(); i++) {
				String elemText = getTableElements.get(i).getText().trim();
				if (elemText.equalsIgnoreCase(TextToSearch)) {
					WebElement Name = waiting.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
							By.xpath("//table[@id='" + Value + "']//tbody//tr//td[" + actionCol + "]"))).get(i);
					Text = Name.getText();
				}
			}
		} catch (Exception e) {
			// System.out.println("exception occured : " + e.getMessage());
			Reporter.log(page + "Exception in getTextFromTable(): " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getTextFromTable(): " + element.toString() + "\n " +
			// e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return Text;
	}

	public String ConfigFileValue(String ParameterName){
		String Value = null;
		try{
			ConfigFileReader();
			Value =properties.getProperty(ParameterName);
		}
		catch(Exception e){
			Reporter.log(page + ": Exception in selectListOption() : " + e.getMessage());
		}
		return Value;
	}

	
	protected String[] getTDData(WebElement el) {
		String[] data = null;

		try {
			// el is ROW
			List<WebElement> cols = el.findElements(By.cssSelector("td"));
			data = new String[cols.size()];
			int i = 0;

			for (WebElement column : cols) {
				data[i] = column.getText();
				i++;
			}
		} catch (Exception e) {
			// System.out.println("exception occured : " + e.getMessage());
			Reporter.log(page + "Exception in getTDData(): " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getTDData(): " + el.toString() + "\n " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return data;
	}

	// el should contain path till TH
	protected int getHeaderColIndex(By el, String columnNameToSearch) {
		int headerColIndex = 1;
		try {
			List<WebElement> colHeaders = driver.findElements(el);
			if (colHeaders.size() > 0) {
				for (WebElement h : colHeaders) {
					// System.out.println("Header text --> " +
					// h.getAttribute("aria-label"));
					if (h.getAttribute("aria-label").trim().contains(columnNameToSearch))
						return headerColIndex;
					else
						headerColIndex++;
				}
			} else {
				// System.out.println("Table header not present: " +
				// el.toString());
				Reporter.log(page + "Table header not present: " + el.toString());
				// extentTest.log(LogStatus.WARNING, " +
				// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
			}
		} catch (Exception e) {
			Reporter.log(page + ": Exception in getHeaderColIndex() : " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getHeaderColIndex(): " + el.toString() + "\n " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return headerColIndex;
	}

	// el - el should contain value till TH
	protected String[] getTDHeaders(By el, String attribute, String splitBy) {
		String[] headers = null;

		try {
			if (isElementPresent(el)) {
				List<WebElement> els = driver.findElements(el);
				headers = new String[els.size()];
				int i = 0;

				for (WebElement e : els) {
					String headerSplit[] = null;
					headerSplit = e.getAttribute(attribute).split(splitBy);
					// System.out.println(headerSplit.length);
					if (!(headerSplit.length == 1)) {
						for (int j = 0; j < headerSplit.length; j++) {
							if (!headerSplit[j].trim().equals("")
									&& !(headerSplit[j].trim().contains("activate to sort column"))) {
								headers[i] = headerSplit[j].trim();
								i++;
							}
						}
					} else {
						headers[i] = headerSplit[0];
					}
				}
			} else {
				// System.out.println("Table header not present: " +
				// el.toString());
				Reporter.log(page + ": Table header not present: " + el.toString());
			}
		} catch (Exception e) {
			// System.out.println("Exception in getTDHeaders() : " +
			// e.getMessage());
			Reporter.log(page + ": Exception in getTDHeaders() : " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// getTDHeaders(): " + el.toString() + "\n " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
		return headers;
	}

	protected boolean waitForElementRefreshPage(By el) throws Exception {
		boolean present = false;
		int attempts = 3;

		while (!present && attempts != 0) {
			try {
				if (driver.findElements(el).size() > 0) {
					present = true;
					// extentTest.log(LogStatus.INFO, "Element found: " +
					// el.toString());
					break;
				} else {
					Thread.sleep(5000);
					present = false;
					// System.out.println("elment not yet found: " + attempts);
					driver.navigate().refresh();
					attempts--;
				}
			} catch (NoSuchElementException e) {
				Reporter.log(page + ": Exception in waitForElementRefreshPage() : " + e.getMessage());
				// extentTest.log(LogStatus.FAIL, page + ": Exception in
				// waitForElementRefreshPage(): " + el.toString() + "\n " +
				// e.getMessage() +
				// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
				return present;
			}
		}
		return present;
	}

	protected boolean isElementPresent(By el) {
		try {
			List<WebElement> l = driver.findElements(el);
			if (l.size() > 0) {
				// System.out.println("Element Present: "+el);
				// extentTest.log(LogStatus.INFO, "Element Present:
				// "+el.toString());
				// .info(methodName + " ---> Element Present: "+el);
				Reporter.log(page + ": Element Present: " + el.toString());
				return true;
			}
		} catch (NoSuchElementException e) {
			Reporter.log(page + ": Exception in isElementPresent() : " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// isElementPresent(): " + el.toString() + "\n " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
			return false;
		}
		return false;
	}

	protected boolean isElementDisplayed(By el) {
		// System.out.println("Executing isElementDisplayed");
		try {
			if (driver.findElement(el).isDisplayed()) {
				// log4j.info(methodName + " ---> Element is displayed: " + el);
				return true;
			}
		} catch (NoSuchElementException e) {
			// log4j.error(methodName + " ---> NoSuchElementException ERROR " +
			// el);
			Reporter.log(page + ": Exception in isElementDisplayed() : " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// isElementDisplayed(): " + el.toString() + "\n " + e.getMessage()
			// +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
			return false;
		}
		return false;
	}

	protected boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			// log4j.info(methodName + " ---> SWITCHED TO ALERT ");
			return true;
		} catch (NoAlertPresentException e) {
			// log4j.error(methodName + " ---> NoAlertPresentException ");
			Reporter.log(page + ": Exception in isAlertPresent() : " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Exception in
			// isElementDisplayed(): " + el.toString() + "\n " + e.getMessage()
			// +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
			return false;
		}
	}

	protected void isElementSelected(By xpath, String element) {
		System.out.println("Executing Select on: " + element + " On Page:" + page);
		// test.log(LogStatus.INFO,"Executing Select on: "+element);
		try {
			boolean val = driver.findElement(xpath).isSelected();
			if (val == true) {
				Reporter.log(page + ": Element not selected: " + xpath.toString());
				// test.log(LogStatus.INFO,element+": is selected.");
				// log4j.info(element+": is selected.");
			} else {
				Reporter.log(page + ": Element is not selected: " + xpath.toString());
			}

		} catch (Exception e) {
			Reporter.log(page + ": Exception in isElementSelected() : " + e.getMessage());
		}
	}

	protected String closeAlertAndGetItsText() {
		// System.out.println("Executing closeAlertAndGetItsText");
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();

			if (acceptNextAlert) {
				alert.accept();
				// log4j.info("Alert is accepted");
			} else {
				alert.dismiss();
				// log4j.info("Alert is dismissed");
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}

	protected void waitForElement(By locator) {
		// System.out.println("Executing waitForElement "+locator);
		WebDriverWait wait = new WebDriverWait(driver, Constants.ELEMENTWAITDURATIONLONG);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			// log4j.info("Element is visible " + locator);
		} catch (Exception e) {
			Reporter.log(page + ": Exception in waitForElement() : " + e.getMessage());
		}
	}
	
	public void waitForElementTobeclickable(By locator) {
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, Constants.ELEMENTWAITDURATIONLONG);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		}
		catch(Exception e)
		{
			System.out.println(page + ": Exception in waitForElementTobeclickable()" + e.getMessage());
		}
		
	}

	protected void waitForElementToDisappear(By locator) {
		System.out.println("Executing waitForElementToDisappear " + locator);
		WebDriverWait wait = new WebDriverWait(driver, Constants.ELEMENTWAITDURATIONLONG);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
			// log4j.info("Element has disappeared: " + locator);
		} catch (Exception e) {
			Reporter.log(page + ": Exception in waitForElementToDisappear() : " + e.getMessage());
		}
	}

	protected void waitForElementSeconds(String element, int sec) {
		// System.out.println("Executing waitForElement "+element);

		// test.log(LogStatus.INFO,"Executing waitForElement "+locator);
		WebDriverWait wait = new WebDriverWait(driver, sec);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(TestUtils.getLocator(element)));
			// log4j.info("Element is visible: " + element);
		} catch (Exception e) {
			Reporter.log(page + ": Exception in waitForElementSeconds() : " + e.getMessage());
		}
	}

	protected void waitAndClickforElement(String element, int sec) throws Exception {
		// System.out.println("Executing wait and click for Element :
		// "+element);

		// test.log(LogStatus.INFO,"Executing waitForElement "+locator);
		WebDriverWait wait = new WebDriverWait(driver, sec);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(TestUtils.getLocator(element)));
			Reporter.log(page + ": Element is clickable: " + element);
		} catch (Exception e) {
			Reporter.log(page + ": Exception in waitAndClickforElement() : " + e.getMessage());
		}
	}

	public void waitFixedTimeSeconds(long t) {
		try {
			Thread.sleep(t * 1000);
			// log4j.info("Waited for Seconds : " + t);
		} catch (Throwable e) {
			Reporter.log(page + ": Exception in waitFixedTimeSeconds() : " + e.getMessage());
		}
	}

	protected void selectListOptionMP(String element, String Value) throws Exception {
		try{
			waitFixedTimeSeconds(1);
			By Element = TestUtils.getLocator(element);
			String value = Element.toString().split(": ")[1];
			String dropDownXpath = "//*[@id='" + value + "']//div//ul//li";
			String textboxxpath = "//div[@id='" + value + "']";
			driver.findElement(By.xpath(textboxxpath)).click();
			List<WebElement> droplist_Contents = driver.findElements(By.xpath(dropDownXpath));
			for (int i = 0; i <= droplist_Contents.size(); i++) {
				String OptionText = droplist_Contents.get(i).getText();
				if (OptionText.equalsIgnoreCase(Value)) {
					if (i != 0) {
						String elemXPath = "//*[@id='" + value + "']//div//ul//li[" + i + "]";
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",
								driver.findElement(By.xpath(elemXPath)));
					}
					//Thread.sleep(2000);
					droplist_Contents.get(i).click();
					extentTest.log(LogStatus.PASS,
							page + ": Able to input data: " + value + " to field: " + textboxxpath);
					break;

				}
		}
		}	
		catch(Exception e){
			Reporter.log(page + ": Exception in selectListOption() : " + e.getMessage());
		}
	}
	
	
	protected void selectListOption(String element, String Value) throws Exception {
		try {
			waitFixedTimeSeconds(1);
			By Element = TestUtils.getLocator(element);
			String value = Element.toString().split(": ")[1];
			String dropDownXpath = "//*[@id='" + value + "']//div[2]//div//span";
			String textboxxpath = "//div[@id='" + value + "']";
			driver.findElement(By.xpath(textboxxpath)).click();
			List<WebElement> droplist_Contents = driver.findElements(By.xpath(dropDownXpath));
			// driver.findElement(By.xpath("//div[@id='clientCountry_chosen']//a//div//div//input")).sendKeys("India");
			for (int i = 0; i <= droplist_Contents.size(); i++) {
				String OptionText = droplist_Contents.get(i).getText();
				if (OptionText.equalsIgnoreCase(Value)) {
					if (i != 0) {
						String elemXPath = "//*[@id='" + value + "']//div[2]//div[" + i + "]//span";
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",
								driver.findElement(By.xpath(elemXPath)));
					}
					//Thread.sleep(2000);
					droplist_Contents.get(i).click();
					extentTest.log(LogStatus.PASS,
							page + ": Able to input data: " + value + " to field: " + textboxxpath);
					break;

				}
			}
		} catch (Exception e) {
			// System.out.println(e);
			Reporter.log(page + ": Exception in selectListOption() : " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, Page + ": EXCEPTION in
			// selectListOption() --> " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
	}

	protected void selectListOption1(String element, String Value) throws Exception {
		try {
			waitFixedTimeSeconds(1);
			By Element = TestUtils.getLocator(element);
			String value = Element.toString().split(": ")[1];
			String dropDownXpath = "//*[@id='" + value + "']//div[2]//div//span";
			String textboxxpath = "//div[@id='" + value + "']";
			driver.findElement(By.xpath(textboxxpath)).click();
			List<WebElement> droplist_Contents = driver.findElements(By.xpath(dropDownXpath));
			// driver.findElement(By.xpath("//div[@id='clientCountry_chosen']//a//div//div//input")).sendKeys("India");
			for (int i = 0; i <= droplist_Contents.size(); i++) {
				String OptionText = droplist_Contents.get(i).getText();
				if (OptionText.contains(Value)) {
					if (i != 0) {
						String elemXPath = "//*[@id='" + value + "']//div[2]//div[" + i + "]//span";
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",
								driver.findElement(By.xpath(elemXPath)));
					}
					Thread.sleep(2000);
					droplist_Contents.get(i).click();
					break;
				}
			}
		} catch (Exception e) {
			// System.out.println(e);
			Reporter.log(page + ": Exception in selectListOption() : " + e.getMessage());
			// extentTest.log(LogStatus.FAIL, Page + ": EXCEPTION in
			// selectListOption() --> " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
	}

	protected void performClickinTable(String element, int SearchColNUm, String TextToSearch, int actionCol)
			throws Exception {
		List<WebElement> getTableElements = null;
		WebDriverWait waiting = new WebDriverWait(driver, 3);
		By Element = TestUtils.getLocator(element);
		String Value = Element.toString().split(": ")[1];

		try {
			getTableElements = driver
					.findElements(By.xpath("//table[@id='" + Value + "']//tbody//tr//td[" + SearchColNUm + "]"));
			for (int i = 0; i < getTableElements.size(); i++) {
				String elemText = getTableElements.get(i).getText().trim();
				if (TextToSearch.equalsIgnoreCase(elemText)) {
					WebElement Name = waiting.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
							By.xpath("//table[@id='" + Value + "']//tbody//tr//td[" + actionCol + "]"))).get(i);
					Name.click();
					break;
				}
			}
		} catch (Exception e) {
			Reporter.log(page + ": Exception in performClickinTable() : " + e.getMessage());
		}
	}

	protected void input(String element, String data) throws Exception {
		Reporter.log(page + "Executing input on: " + element);
		try {
			driver.findElement(TestUtils.getLocator(element)).clear();
			//Thread.sleep(1000);
			driver.findElement(TestUtils.getLocator(element)).sendKeys(data);
			// log4j.info("Able to input data: "+data+" to field: "+element);
			extentTest.log(LogStatus.PASS, page + ": Able to input data: " + data + " to field: " + element);
			// Reporter.log(page + "Executing input on: "+element);
			// System.out.println("Able to input data: "+data+" to field:
			// "+element);
		} catch (Exception e) {
			Reporter.log(page + ": Exception in input() : " + element + "\n" + e.getMessage());
			// extentTest.log(LogStatus.FAIL, page + ": Element not found: " +
			// element + " EXCEPTION --> " + e.getMessage() +
			// extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
	}

	protected boolean isEnabled(String element) throws Exception {
		Boolean value = false;
		try{
			value = driver.findElement(TestUtils.getLocator(element)).isEnabled();
		}
		catch(Exception e){
			Reporter.log(page + ": Exception in input() : " + element + "\n" + e.getMessage());
		}
		return value;
	}
	
	protected String getText(String element) throws Exception {
		// test.log(LogStatus.INFO,"Executing input on: "+element+" On
		// Page:"+Page+" Input Data:"+data);
		String Value = null;
		try {

			Value = driver.findElement(TestUtils.getLocator(element)).getText();
			// log4j.info("Able to input data: "+data+" to field: "+element);
			extentTest.log(LogStatus.PASS, page + ": Able to get: " + Value + " to field: " + element);
			// System.out.println("Able to input data: "+data+" to field:
			// "+element);
		} catch (Exception e) {
			// log4j.error("Element not found: "+element+"<>INPUT-FAIL<>");
			// System.out.println("Element not found: "+element);
			extentTest.log(LogStatus.FAIL,
					page + ": Exception in getText(): Element not found: " + element + " EXCEPTION --> "
							+ e.getMessage() + extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}

		return Value;
	}

	protected String getValue(String element) throws Exception {
		// test.log(LogStatus.INFO,"Executing input on: "+element+" On
		// Page:"+Page+" Input Data:"+data);
		String Value = null;
		try {

			Value = driver.findElement(TestUtils.getLocator(element)).getAttribute("value");
			// log4j.info("Able to input data: "+data+" to field: "+element);
			extentTest.log(LogStatus.PASS, page + ": Able to get: " + Value + " to field: " + element);
			// System.out.println("Able to input data: "+data+" to field:
			// "+element);
		} catch (Exception e) {
			// log4j.error("Element not found: "+element+"<>INPUT-FAIL<>");
			// System.out.println("Element not found: "+element);
			extentTest.log(LogStatus.FAIL,
					page + ": Exception in getText(): Element not found: " + element + " EXCEPTION --> "
							+ e.getMessage() + extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}

		return Value;
	}

	protected void click(String element) throws Exception {
		System.out.println("Executing click on: " + element);
		// test.log(LogStatus.INFO,"Executing click on: "+element+" On
		// Page:"+Page);
		try {
			driver.findElement(TestUtils.getLocator(element)).click();
			extentTest.log(LogStatus.PASS, page + ": Able to click on: " + element);
			// log4j.info();
		} catch (Exception e) {
			// log4j.error("Element not found: "+element);
			extentTest.log(LogStatus.FAIL,
					page + ": Exception in click(): Element not found: " + element + " EXCEPTION --> " + e.getMessage()
							+ extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
		}
	}

	protected boolean verifyText(String element, String expectedText) throws Exception {
		System.out.println("Executing verifyText() on Element:" + element + " On Page:" + page);
		// test.log(LogStatus.INFO,"Executing verifyText on Element:"+element+"
		// On Page:"+Page);
		try {
			// WebElement element1=driver.findElement(xpath);
			String actualText = driver.findElement(TestUtils.getLocator(element)).getText();
			// System.out.println(actualText);
			if (actualText.equals("")) {
				// System.out.println("Generate access code message not
				// displayed");
				extentTest.log(LogStatus.WARNING,
						page + ": Generate access code message not displayed. Actual text displayed: " + actualText);
				return false;
			}
			if (expectedText.trim().equalsIgnoreCase(actualText.trim())) {
				System.out.println("Actual text matching Expected value");
				// log4j.info("Actual text matching Expected value for: " +
				// element);
				extentTest.log(LogStatus.PASS,
						page + ": Generate Access Code Message displayed. Actual text displayed: " + actualText);
				return true;
			} else {
				System.out.println("Actual text not matching Expected value");
				// log4j.info("Actual text not matching Expected value for:
				// "+element);
				extentTest.log(LogStatus.WARNING,
						page + ": Generate Access Code Message displayed. But not matching. ACTUAL TEXT: " + actualText
								+ " EXPECTED TEXT: " + expectedText);
				return false;
			}
		} catch (Exception e) {
			// System.out.println("Not found: "+element);
			extentTest.log(LogStatus.FAIL,
					page + ": Exception in verifyText(): Message element not found: " + element + "EXCEPTION: "
							+ e.getMessage() + extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
			return false;
		}
	}

	protected boolean verifyTextonPage(String expectedText) throws Exception {
		// System.out.println("Executing verifyTextonPage() on Element:" +
		// expectedText+" On Page:"+page);
		// test.log(LogStatus.INFO,"Executing verifyText on Element:"+element+"
		// On Page:"+Page);
		try {
			// WebElement element1=driver.findElement(xpath);
			String pgSource = driver.getPageSource();
			if (pgSource.contains(expectedText)) {
				System.out.println("Page contains Expected text");
				// log4j.info("Actual text matching Expected value for: " +
				// element);
				extentTest.log(LogStatus.PASS, page + ": Page contains expected text: " + expectedText);
				return true;
			} else {
				System.out.println("Page doesnt contain expected text: " + expectedText);
				// log4j.info("Actual text not matching Expected value for:
				// "+element);
				extentTest.log(LogStatus.WARNING, page + ": Page doesnt contain expected text: " + expectedText);
				return false;
			}
		} catch (Exception e) {
			// System.out.println("Expected text Not found: " + expectedText);
			extentTest.log(LogStatus.FAIL,
					page + ": Exception in verifyTextonPage(): Expected text not found or Issue with page: "
							+ expectedText + " EXCEPTION: " + e.getMessage()
							+ extentTest.addScreenCapture(ExtentManager.takeScreenshot(driver)));
			return false;
		}
	}

	protected boolean verifyErrorText(By errorID, By xpath, String expectedText, String element, String Page) {
		// test.log(LogStatus.INFO,"Executing verifyText() on
		// Element:"+element+" On Page:"+Page);
		if (ifErrorExists(errorID)) {
			try {
				// WebElement element1=driver.findElement(xpath);
				String actualText = driver.findElement(xpath).getText();

				if (expectedText.trim().equals(actualText.trim())) {
					Reporter.log(page + ": Actual text matching Expected value");
					Reporter.log(page + ": Actual text -- " + actualText);
					Reporter.log(page + ": Expected text -- " + expectedText);
					// test.log(LogStatus.INFO,"Actual text matching Expected
					// value");
					// log4j.info("Actual Error text matching Expected value
					// for: "+element);
					return true;
				} else {
					Reporter.log(page + ": Actual text NOT matching Expected value");
					Reporter.log(page + ": Actual text -- " + actualText);
					Reporter.log(page + ": Expected text -- " + expectedText);
					return false;
				}
			} catch (Exception e) {
				Reporter.log(page + ": Exception in verifyErrorText() : " + xpath.toString() + "\n" + e.getMessage());
				// test.log(LogStatus.INFO,"Not found: "+element);
				// Reports.takeSnapshot(element+" not
				// displayed<>ASSERT-FAIL<>"+Page+"<>*",FAIL, driver);
				// log4j.error("Exception occured while verifying Error code " +
				// element + "--> " + e.getMessage());
			}
		}
		return false;
	}

	/*
	 * static FileHandler fileTxt; static SimpleFormatter formatterTxt;
	 * protected void logger(String message) throws IOException {
	 * 
	 * final Logger logger = Logger.getLogger("");
	 * logger.setLevel(Level.INFO);//Loget Info, Warning dhe Severe do ruhen
	 * fileTxt = new FileHandler(Constants.LOGFILEPATH); formatterTxt = new
	 * SimpleFormatter(); fileTxt.setFormatter(formatterTxt);
	 * logger.addHandler(fileTxt); logger.info(message);
	 * 
	 * }
	 */

	// Verify if error exists
	protected boolean ifErrorExists(By xpath) {
		// test.log(LogStatus.INFO,"Executing ifErrorExists");
		try {

			if (driver.findElement(xpath).getAttribute("style").contains("block")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			// test.log(LogStatus.INFO,"Not found: "+xpath);
			Reporter.log(page + ": Exception in ifErrorExists() : " + xpath.toString() + "\n" + e.getMessage());
			return false;
		}
	}

}

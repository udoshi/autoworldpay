package tests.base;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.remote.BrowserType;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import com.google.common.base.Function;

import instarem.utilities.Constants;

public class TestBase {

	protected RemoteWebDriver _driver = null;
	protected AndroidDriver<WebElement> _driver1 = null;
    public static String BrowserType = "Web";
	public static String getDate() {
		Date now = new Date();
		// String datetimeStr = now.toString();
		SimpleDateFormat format = new SimpleDateFormat("MMM_dd_yyyy");
		return (format.format(now));
	}

	public static String getTime() {
		Date now = new Date();
		// String datetimeStr = now.toString();
		SimpleDateFormat format = new SimpleDateFormat("HH_mm");
		return (format.format(now));
	}

	public static int getRandomNo() {
		Random rand = new Random();
		int n = rand.nextInt(10000) + 1;
		return n;
		// 50 is the maximum and the 1 is our minimum
	}

	public static String dateAndTime() {

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String po = dateFormat.format(date);
		System.out.println(po);
		return po;
	}

	public void useFluentWait(WebDriver driver) {
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)

		       .withTimeout(30, TimeUnit.SECONDS)

		       .pollingEvery(5, TimeUnit.SECONDS)

		       .ignoring(NoSuchElementException.class);

	   
			WebElement foo = wait.until(new Function<WebDriver, WebElement>() {

		     public WebElement apply(WebDriver driver) {

		       return driver.findElement(By.id("Element"));

		     }
		   });
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public RemoteWebDriver SetBrowser(String browser) throws Exception {
		DesiredCapabilities cap = null;

		if (browser.contentEquals("firefoxgrid")) {
			// System.setProperty("Webdriver.gecko.driver",
			// Constants.FIREFOXDRIVERPATH);
			// _driver = new FirefoxDriver();
			// FirefoxProfile firefoxProfile = new FirefoxProfile();
			// firefoxProfile.setAssumeUntrustedCertificateIssuer(false);
			cap = DesiredCapabilities.firefox();
			_driver = new RemoteWebDriver(new URL(Constants.SELENIUM_GRID_URL), cap);
			_driver.manage().window().maximize();
			_driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
		if (browser.contentEquals("firefox")) {
			System.setProperty("webdriver.gecko.driver", Constants.FIREFOXDRIVERPATH);
			_driver = new FirefoxDriver();
			_driver.manage().window().maximize();
			_driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} else if (browser.contentEquals("ie")) {
			System.setProperty("Webdriver.ie.driver", Constants.IEDRIVERPATH);
			_driver = new InternetExplorerDriver();
			_driver.manage().window().maximize();
			_driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}else if (browser.contains("MobileChrome")){
			BrowserType = "Mobile";
			DesiredCapabilities capabilities = new DesiredCapabilities();
			// set the capability to execute test in chrome browser
			capabilities.setCapability(MobileCapabilityType.BROWSER_NAME,"Chrome");
			capabilities.setCapability("deviceName", "ZY322JMBKT");
			capabilities.setCapability("platformVersion", "7.1.1");
			capabilities.setCapability("platformName", "Android");
			/*capabilities.setCapability("app", app.getAbsolutePath());*/
			//capabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");
			//capabilities.setCapability("appActivity", "com.amazon.mShop.home.HomeActivity");
			_driver1 = new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
		} 
		else if (browser.contentEquals("chromewebcam")){
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			options.addArguments("user-data-dir=F:/Automation/autosel");
			System.setProperty("webdriver.chrome.driver",Constants.CHROMEDRIVERPATH);
			_driver =new ChromeDriver(options);
		}
		else if (browser.contentEquals("chrome")) {
			//Now initialize chrome driver with chrome options which will switch off this browser notification on the chrome browser
			System.setProperty("webdriver.chrome.driver", Constants.CHROMEDRIVERPATH);
			String downloadFilepath = Constants.DOWNLOADFILE;
			Map<String, Object> preferences = new Hashtable<String, Object>();
			preferences.put("profile.default_content_settings.popups", 0);
			//preferences.put("download.prompt_for_download", "false");
			preferences.put("download.default_directory", downloadFilepath);
			// disable flash and the PDF viewer
			preferences.put("plugins.plugins_disabled", new String[]{
			    "Adobe Flash Player", "Chrome PDF Viewer"});
			preferences.put("profile.default_content_setting_values.notifications", 2);
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", preferences);
			options.addArguments("disable-popup-blocking");
			options.addArguments("--browser.download.folderList=2");
			options.addArguments("--browser.helperApps.neverAsk.openFile=text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml,application/zip, application/octet-stream,application/x-rar-compressed, application/octet-stream");
			options.addArguments("--browser.helperApps.neverAsk.saveToDisk=text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml,application/zip, application/octet-stream,application/x-rar-compressed, application/octet-stream");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			System.out.println(Constants.CHROMEDRIVERPATH);
			_driver = new ChromeDriver(options);
			_driver.manage().deleteAllCookies();
			_driver.manage().window().maximize();
			_driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} else if (browser.contentEquals("chromegrid")) {
			cap = DesiredCapabilities.chrome();
			cap.setPlatform(Platform.WINDOWS);
			_driver = new RemoteWebDriver(new URL(Constants.SELENIUM_GRID_URL), cap);
		} else if (browser.equalsIgnoreCase("linuxChrome")) {
			System.setProperty("webdriver.chrome.driver", Constants.LINUXCHORMEDRIVERPATH);
			ChromeOptions options = new ChromeOptions();
			options.setHeadless(true);
			options.addArguments("disable-gpu");
			options.addArguments("window-size=1200,1100");
			_driver = new ChromeDriver(options);
			_driver.manage().window().maximize();
			_driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

		if(BrowserType.contains("MobileChrome")){
			return(_driver1);
		}
		else{
			return (_driver);
		}
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public AndroidDriver<WebElement> setBrowserMobile(String browser) throws Exception{
		if (browser.contains("MobileChrome")){
			BrowserType = "Mobile";
			DesiredCapabilities capabilities = new DesiredCapabilities();
			// set the capability to execute test in chrome browser
			capabilities.setCapability(MobileCapabilityType.BROWSER_NAME,"Chrome");
			capabilities.setCapability("deviceName", "ZY322JMBKT");
			capabilities.setCapability("platformVersion", "7.1.1");
			capabilities.setCapability("platformName", "Android");
			/*capabilities.setCapability("app", app.getAbsolutePath());*/
			//capabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");
			//capabilities.setCapability("appActivity", "com.amazon.mShop.home.HomeActivity");
			_driver1 = new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
		} 
		return (_driver1);
	}

	@SuppressWarnings("restriction")
	public void sendMailWithAttachment(String subject, String msgBody, String imagePath,
			String toAddressCommaSeparated) {

		Properties props = new Properties();
		// props.put("mail.smtp.host", "smtp.gmail.com"); //smtp.mailgun.org
		props.put("mail.smtp.host", "smtp.mailgun.org"); //
		// props.put("mail.smtp.starttls.enable", "true");
		// props.put("mail.smtp.user", "insta7135@instarem.com");
		// //postmaster@alts.instarem.com
		props.put("mail.smtp.user", "postmaster@alts.instarem.com"); //
		// props.put("mail.smtp.password", "Smart100");
		// //85e2b4e76008dae23dfed0c01ba87eae
		props.put("mail.smtp.password", "85e2b4e76008dae23dfed0c01ba87eae");
		// props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");

		// System.out.println("Inside send email");

		Session session = Session.getDefaultInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("postmaster@alts.instarem.com", "85e2b4e76008dae23dfed0c01ba87eae");
			}
		});

		Message message = new MimeMessage(session);

		// System.out.println("creating new session..");

		try {
			message.setFrom(new InternetAddress("postmaster@alts.instarem.com"));
			InternetAddress multipleRecipients = new InternetAddress();
			message.addRecipients(Message.RecipientType.TO, multipleRecipients.parse(toAddressCommaSeparated));
			message.setSubject(subject);
			BodyPart messageBodyPart1 = new MimeBodyPart();
			messageBodyPart1.setText(msgBody);
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			messageBodyPart2.attachFile(new File(imagePath));
			// System.out.println("Msgbody is set.. ");
			/*
			 * DataSource source = new FileDataSource(imagePath);
			 * messageBodyPart2.setDataHandler(new DataHandler(source));
			 * messageBodyPart2.setFileName("Results.html");
			 */
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart2);
			multipart.addBodyPart(messageBodyPart1);
			message.setContent(multipart);
			// System.out.println("Added to the msg...");
			Transport transport = session.getTransport("smtp");
			// transport.connect("smtp.gmail.com", "insta7135@gmail.com",
			// "Smart100");
			transport.connect("smtp.mailgun.org", "postmaster@alts.instarem.com", "85e2b4e76008dae23dfed0c01ba87eae");
			Transport.send(message, message.getAllRecipients());
			transport.close();
			System.out.println("Mail sent");
		} catch (Exception e) {
			// JLogger.log(Level.SEVERE, "Exception in sendMailWithAttachment():
			// " +e.getMessage());
			System.out.println("Exception in sendMailWithAttachment(): \n" + e.getMessage());
			// e.printStackTrace();
		}

	}

	// Commited on 23/11/2017 method of set global variable
	@SuppressWarnings("unchecked")
	public void setGlobalVariable(String KeyName, String KeyValue) throws Exception {
		// Initialize json parser to read the file
		JSONParser jsonParser = new JSONParser();
		try (FileReader reader = new FileReader(Constants.JSONFILEPATH)) {
			// Read JSON file after creating object
			Object obj = jsonParser.parse(reader);
			JSONObject Object = (JSONObject) obj;
			Object.remove(KeyName);
			Object.put(KeyName, KeyValue);
			FileWriter file = new FileWriter(Constants.JSONFILEPATH);
			// writing new values into the file
			file.write(Object.toJSONString());
			file.flush();
			file.close();
		} catch (Exception e) {
			System.out.println("Error in setGlobalVariable()" + e.getMessage());
		}
	}

	// Method to get global variable
	public String getGlobalVariable(String KeyName) throws Exception {
		String value = null;
		org.json.simple.JSONObject jsonObject = null;
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(Constants.JSONFILEPATH));
			jsonObject = (org.json.simple.JSONObject) obj;
			value = (String) jsonObject.get(KeyName);
		} catch (Exception e) {
			System.out.println("Error in getGlobalVariable()" + e.getMessage());
		}
		return value;
	}
	
	public int verifyLinkActive(String linkUrl) {
		int responseCode = 0;
		int linkBroken = 0;
		try {
			URL url = new URL(linkUrl);

			HttpURLConnection httpURLConnect = (HttpURLConnection) url.openConnection();

			httpURLConnect.setConnectTimeout(3000);

			httpURLConnect.connect();

			if (httpURLConnect.getResponseCode() == 200) {

				System.out.println(linkUrl + " - " + httpURLConnect.getResponseMessage());
				responseCode = httpURLConnect.getResponseCode();
				return responseCode;
			}
			if (httpURLConnect.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
				System.out.println(linkUrl + " - " + httpURLConnect.getResponseMessage() + " - "
						+ HttpURLConnection.HTTP_NOT_FOUND);
				responseCode = httpURLConnect.getResponseCode();
				return responseCode;

			}
			if (httpURLConnect.getResponseCode() != 200) {
				linkBroken = linkBroken + 1;
				System.out.println(linkUrl + " - " + httpURLConnect.getResponseMessage() + " - " + "Respnse Code is "
						+ httpURLConnect.getResponseCode());
				responseCode = httpURLConnect.getResponseCode();
				//return responseCode;
			}

		} catch (Exception e) {

			return responseCode;

		}
		return responseCode;

	}
}

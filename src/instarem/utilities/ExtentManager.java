package instarem.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;

public class ExtentManager {
	
	public static ExtentReports extent;
	public static ExtentTest logger;
	
	public static ExtentReports getExtentReport()
	{
		if (extent == null)
		{
			String reportName = Constants.REPORTPATH + "Report_" + getDateTime() + ".html";
			//System.out.println(reportName);
			Constants.REPORTFILENAME = reportName;
			//System.out.println(Constants.EXTENTCONFIGPATH);
			File reportFile = new File(reportName);
			extent = new ExtentReports (reportFile.getAbsolutePath(), true);
			if (reportFile.exists())
				reportFile.delete();
			extent.addSystemInfo("Host Name", "Instarem Testing");
			extent.addSystemInfo("Environment", "Automation Testing");
			extent.addSystemInfo("User Name", "Priya Thakur");
			//ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportFile);
			extent.loadConfig(new File(Constants.EXTENTCONFIGPATH));
			//System.out.println("config file loaded");
			
			// LOG4J disabled for now
			//logInit(); 					
		}
		return extent;
	}
	
	/*public static void logInit()
	{
		log4j = Logger.getLogger(Log.class.getName());
		DOMConfigurator.configure(Constants.logFile);
	}*/
	
	public static ExtentTest getTestLogger(String methodName)
	{
		logger = extent.startTest(methodName);
		return logger;
	}
	
	public static void getScreenshotPass(String message, RemoteWebDriver driver) throws IOException
	{
		logger.log(LogStatus.PASS, message + logger.addScreenCapture(takeScreenshot(driver)));
	}
	
	public static void getScreenshot(String message, RemoteWebDriver driver) throws IOException
	{
		logger.log(LogStatus.FAIL, message + logger.addScreenCapture(takeScreenshot(driver)));
	}
	
	public static String takeScreenshot(RemoteWebDriver driver)
	{
		File dest=null;
		try
		{
			File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			//System.out.println(src.getAbsoluteFile());
			dest = new File (Constants.IMAGESPATH + getImageName());
			//System.out.println(dest.getAbsolutePath());
			FileUtils.copyFile(src, dest);
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Screenshot couldnt be taken" + e.getMessage());
			//e.printStackTrace();
		}
		return dest.getAbsolutePath();
	}
	
	public static String getDateTime()
	{
		Date now = new Date();
        //String datetimeStr = now.toString();
        SimpleDateFormat format = new SimpleDateFormat("MMM_dd_yyyy_HH_mm_ss_zzz");
        //System.out.println(format.format(now));
        return (format.format(now));
	}

	
	public static String getImageName()
	{
		String imageName = "Image";
		return (imageName+ getDateTime() + ".png");
	}
	
	public static void WriteToReport()
	{
		extent.flush();
		//extent.close();
	}
	//Date date = new Date().get;
	
	
}

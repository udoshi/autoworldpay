package instarem.utilities;


import java.util.List;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestNGMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import instarem.utilities.Constants;
import instarem.utilities.ExtentManager;
import instarem.utilities.TestUtils;
import tests.base.TestBase;

public class SendMail extends TestBase {
	
	private static ExtentReports testReport=null;
	private static ExtentTest extentTest;
	private String msgBody = "";
	
	@Test(enabled=true)
	public void sendMailExtentReport() throws Exception
	{
		testReport = ExtentManager.getExtentReport();
		extentTest = ExtentManager.getTestLogger(Constants.TESTMETHODNAME);
		try
		{
			//System.out.println("file to be sent - " + Constants.REPORTFILENAME);
			int passedPercentage=0;
			
			msgBody = "Test Execution Report\n\n";
			
			int total = Constants.PASSEDTESTS.size() + Constants.FAILEDTESTS.size() + Constants.SKIPPEDTESTS.size();
			if (Constants.PASSEDTESTS.size()!=0)
				passedPercentage = (100*Constants.PASSEDTESTS.size())/total;
			else
				passedPercentage = 0;
			
			msgBody = msgBody + "------------- Passing %: " + passedPercentage + "\n\n";
			
			msgBody = msgBody + " ------------- Total Passed Tests: " + Constants.PASSEDTESTS.size()+ "\n";
			getDetails(Constants.PASSEDTESTS);
			
			msgBody = msgBody + "\n\n ------------- Total Failed Tests: " + Constants.FAILEDTESTS.size()+ "\n";
			getDetails(Constants.FAILEDTESTS);
			
			msgBody = msgBody + "\n\n ------------- Total Skipped Tests: " + Constants.SKIPPEDTESTS.size()+ "\n";
			getDetails(Constants.SKIPPEDTESTS);
			
			msgBody = msgBody + "\n ------------- Report File: " + Constants.REPORTFILENAME;
			//System.out.println(msgBody);
			
			sendMailWithAttachment("Test Execution Report", msgBody, Constants.REPORTFILENAME, "pthakur@instarem.com,udoshi@instarem.com,ppatil@instarem.com");
		}
		catch(Exception e)
		{
			extentTest.log(LogStatus.ERROR, "Exception in sendMailExtentReport() :" + e.getMessage());
			e.printStackTrace();
		}
	}

	public void getDetails(List<ITestNGMethod> list)
	{
		if (list.size()!=0)
		{
			for (int i=0; i<list.size(); i++)
				msgBody = msgBody + "\n Method ("+i+"): " + list.get(i).getMethodName();
		}
		else
		{
			msgBody = msgBody + "\nCount=0";
		}
	}
	
}


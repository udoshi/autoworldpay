package instarem.utilities;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import org.openqa.selenium.By;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import tests.base.TestBase;
import java.util.Iterator;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class TestUtils {
	
	/*############################################################
	 * ' Function Name: GetProperties 
	 * ' Purpose: To load OR
	 * ' Inputs Parameters: Object identifier
	 * ' Returns: object identifier
	 * ' Creation Date:05/06/15
	 */
	public static By getLocator(String strObject) throws IOException
	{
		By x=null;
		String locator="";
		String value="";
			
		try
		{
			String[] intermediateValue=strObject.split("\\|");
			for(int i=0;i<intermediateValue.length;i++)
			{
				if (i==0)
					locator=intermediateValue[i];
				else if(i==1)
					value=intermediateValue[i];
			}
			
			System.out.println("Locator -->" + locator);
			System.out.println("Value --> " + value);
			
			if(locator.trim().equalsIgnoreCase("id"))
			     x=By.id(value);
			else if(locator.trim().equalsIgnoreCase("xpath"))
			     x=By.xpath(value);
			else if(locator.trim().equalsIgnoreCase("name"))
			     x=By.name(value);
			else if(locator.trim().equalsIgnoreCase("classname"))
			     x=By.className(value);
			else if(locator.trim().equalsIgnoreCase("cssSelector"))
			     x=By.cssSelector(value);
			else if(locator.trim().equalsIgnoreCase("linkText"))
			     x=By.linkText(value);
			else if(locator.trim().equalsIgnoreCase("partialLinkText"))
				x=By.partialLinkText(value);
			else if(locator.trim().equalsIgnoreCase("tagname"))
				x=By.tagName(value);
		}
		catch(Exception e)
		{
			System.out.println("Issue found with input string -- > " + strObject);
		}
		System.out.println("Locator->"+x);
		return x;
	}
}			



	
	


package instarem.utilities;

import java.util.ArrayList;
import java.util.List;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

public class TestStatistics implements ITestListener {

	public List<ITestNGMethod> passedtests = new ArrayList<ITestNGMethod>();
	public List<ITestNGMethod> failedtests = new ArrayList<ITestNGMethod>();
	public List<ITestNGMethod> skippedtests = new ArrayList<ITestNGMethod>();
	
	//@Override
	public void onTestSuccess(ITestResult result) {
	
	   //add the passed tests to the passed list
		//System.out.println("Inside Test success method: " + result.getTestName());
	
	    Constants.PASSEDTESTS.add(result.getMethod());
	    System.out.println("Successful Method --> " + result.getMethod());
	    passedtests.add(result.getMethod());
	    System.out.println();
	}

    public void onTestFailure(ITestResult result) {

        //add the failed tests to the failed list
    	//System.out.println("Inside Test failed method: " + result.getTestName());
    	Constants.FAILEDTESTS.add(result.getMethod());
        //System.out.println("Failed: " + Constants.FAILEDTESTS.size());
    }

     public void onTestSkipped(ITestResult result) {

         //add the skipped tests to the skipped list
    	//System.out.println("Inside Test skipped method: " + result.getTestName());
        Constants.SKIPPEDTESTS.add(result.getMethod());
        //System.out.println("Skipped: " + Constants.SKIPPEDTESTS.size());
    }

	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext arg0) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}

	public void onTestStart(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}       
}
